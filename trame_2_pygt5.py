#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 4 16:26:16 2023

@author: spasquet
"""

from trame.app import get_server
from trame.ui.vuetify import SinglePageLayout
from trame.widgets import vuetify
import numpy as np

import pyvista as pv
from pyvista.trame.ui import plotter_ui
from pv_utils import pvCoverageAlpha
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QCheckBox
# -----------------------------------------------------------------------------
# Trame initialization
# -----------------------------------------------------------------------------

pv.OFF_SCREEN = True

server = get_server()
state, ctrl = server.state, server.controller

state.trame__title = "Hplus fonctionnaire"
ctrl.on_server_ready.add(ctrl.view_update)

# -----------------------------------------------------------------------------
# Create object for the entire code
# -----------------------------------------------------------------------------

#obj tab pour stocker les noms de fichiers 
file_tot = ["vtk_files/ert1_DefaultInversion_shift.vtk","vtk_files/ert2_DefaultInversion_shift.vtk","vtk_files/ert3_DefaultInversion_shift.vtk","vtk_files/ert4_DefaultInversion_shift.vtk"]

#crea des mesh par application fonction sur le tab des noms de fichiers
mesh = pv.read(file_tot)

#color
my_colormap = 'Spectral_r'

#empty list for the application of coverafeAlpha
C = ['0','0','0','0']

#empty list for stocking pl.add_mesh and checkboxs
actor =[]
checkboxes = []


for j in range(len(mesh)):
    C[j]= pvCoverageAlpha(mesh[j])

### PLot with log colorscale

mesh_actor = None
# -----------------------------------------------------------------------------
# Plotting
# -----------------------------------------------------------------------------
pl = pv.Plotter(window_size=(800, 600))
pl.set_background('lightgrey')
pl.show_bounds(
    grid='front',
    location='outer',
    all_edges=True,
    xlabel="Easting UTM 31N (m)",
    ylabel="Northing UTM 31N (m)",
    zlabel="Elevation (m)",
    color='black',
    fmt='%3i'
)

app = QApplication([])
# Création de la fenêtre principale
widget = QWidget()
# création du gestionnaire de mise en forme
layout_ = QVBoxLayout()




def create_mesh(solid):

    global pl, mesh_actor
    my_colormap = 'Spectral_r'
   # my_colormap = 'cividis'
    if mesh_actor is not None:
        pl.remove_actor(mesh_actor)
    if solid == 'profile':  
        #ajout des maillages a la fenetre
        print('ajout des maillages a la fenetre')
        for i in range(len(mesh)):
           actor.append(pl.add_mesh(mesh[i],scalars='Resistivity',cmap=my_colormap,opacity=C[i],use_transparency=True,))
           
    pl.reset_camera()       

# Fonction de slot pour contrôler l'affichage des maillages
def checkbox_state_changed():
    for j in range(len(mesh)):
        print(j)
        actor[j].SetVisibility(checkboxes[j].isChecked())
        print('gérer le chgmt etat')
    pl.render()

def create_checkbox():
    #creation de toutes les checkbox
    print('creation de toutes les checkbox')

    for i in range(len(mesh)):
        print('boucle des cases')
        checkbox = QCheckBox(file_tot[i])
        checkboxes.append(checkbox)
        
        

def connect_boxes():
    for checkbox in checkboxes:
        checkbox.stateChanged.connect(checkbox_state_changed)
        layout_.addWidget(checkbox)
        
        


# pl.show()


# on fixe le gestionnaire de mise en forme de la fenêtre
# widget.setLayout(layout_)

def essai ():
    print('essai')




with SinglePageLayout(server) as layout:
    layout.title.set_text(state.trame__title)
    # create_mesh("profile")
    # taille_actor = len(actor)
    # print('taille actors')
    # print(taille_actor)
    # create_checkbox()
    # taille_check = len(checkboxes)
    # print('taille checkboxes')
    # print(taille_check)
    
    # connect_boxes()
    # widget.show()

    # # Lancement de l'application
    # app.exec_()
    with layout.toolbar:
        
        vuetify.VSpacer()
        
        vuetify.VBtn("Profile", click=lambda: create_mesh("profile"))
        vuetify.checkbox("essai", click=lambda : essai())

    with layout.content:
        with vuetify.VContainer(
            fluid=True,
            classes="pa-0 fill-height",
        ):
            # Use PyVista UI template for Plotters
            view = plotter_ui(pl)
            ctrl.view_update = view.update

server.start()