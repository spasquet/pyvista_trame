#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 4 16:26:16 2023

@author: spasquet
"""

from trame.app import get_server
from trame.ui.vuetify import SinglePageLayout
from trame.widgets import vuetify
import numpy as np

import pyvista as pv
from pyvista.trame.ui import plotter_ui
from pv_utils import pvCoverageAlpha

import ee
# -----------------------------------------------------------------------------
# Trame initialization
# -----------------------------------------------------------------------------

pv.OFF_SCREEN = True

server = get_server()
state, ctrl = server.state, server.controller

state.trame__title = "Hplus fonctionnaire"
ctrl.on_server_ready.add(ctrl.view_update)


# -----------------------------------------------------------------------------
# Plotting
# -----------------------------------------------------------------------------
#file1 = "vtk_files/WS_DD_profil1_Fonctionnaire_DefaultInversion_shift.vtk"
#file2 = "vtk_files/WS_DD_profil2_Fonctionnaire_DefaultInversion_shift.vtk"

#obj tab pour stocker les noms de fichiers 
file_tot = ["vtk_files/Larzac_Jasse_bis_P01_25m_071010_DefaultInversion_shift.vtk"]

topofile = ['topo/LarzacJasseERT_topo.txt']

#crea des mesh par application fonction sur le tab des noms de fichiers
mesh = pv.read(file_tot)
taille =len(mesh)
print(taille)

#init opacity topo
opa_topo = 0.5

# mesh1 = pv.read(file1) 
# mesh2 = pv.read(file2) 
C = ['0','0','0','0']

for j in range(len(mesh)):
    C[j]= pvCoverageAlpha(mesh[j])

#C1 = pvCoverageAlpha(mesh1)
#C2 = pvCoverageAlpha(mesh2)


pl = pv.Plotter(window_size=(800, 600))
pl.set_background('lightgrey')
pl.show_bounds(
    grid='front',
    location='outer',
    all_edges=True,
    xlabel="Easting UTM 31N (m)",
    ylabel="Northing UTM 31N (m)",
    zlabel="Elevation (m)",
    color='black',
    fmt='%3i'
)
# pl.add_ruler(label_color='black',tick_color='black')

### PLot with log colorscale

mesh_actor = None
#colors list for buttons
colors = [
    'blue',
    # 'red',
    # 'green',
    # 'yellow',
    
]
taille_color = len(colors)
print(taille_color)

class SetVisibilityCallback:
    """Helper callback to keep a reference to the actor being modified."""

    def __init__(self, actor):
        self.actor = actor

    def __call__(self, state):
        self.actor.SetVisibility(state)
        
        
######Coordonnees GPS 
utm_east = []
utm_north = []
gps_1 = []
gps_2 = []
gps_3 = []
gps_4 = []         
def gps_coordonnee():
            
    for j in range(taille):
        # Ouvrir le fichier en lecture seule
        file = open(topofile[j], "r")
        # utiliser readlines pour lire toutes les lignes du fichier
        # La variable "lignes" est une liste contenant toutes les lignes du fichier
        lines = file.readlines()
        # fermez le fichier après avoir lu les lignes
        file.close()

        #decoupe la premiere ligne
        first = lines[0].split()

        #decoupe de la derniere ligne
        num_end = len(lines)
        print(num_end)
        end  = lines[num_end-1].split()
        #recuperation des coordonnees east et north
        print(first[1])
        utm_east.insert(j,float(first[1]))
        utm_north.insert(j,float(first[2]))

        utm_east.insert(j,float(end[1]))
        utm_north.insert(j,float(end[2]))
    print(utm_east)
    print(utm_north)
    gps_1.insert(1,float(min(utm_east)))
    gps_1.insert(2,float(max(utm_north)))
    gps_2.insert(1,float(max(utm_east)))
    gps_2.insert(2,float(max(utm_north)))
    gps_3.insert(1,float(min(utm_east)))
    gps_3.insert(2,float(min(utm_north)))
    gps_4.insert(1,float(max(utm_east)))
    gps_4.insert(2,float(min(utm_north)))
    print(gps_1)
    print(gps_2)
    print(gps_3)
    print(gps_4)



  
def importation():
    #aller recup les donnees avec adresse sur EE, ici celle de la france, voir le site https://developers.google.com/earth-engine/datasets/catalog
    dataset_MNT = ee.Image('IGN/RGE_ALTI/1M/2_0')
    #select uniquement les donnees du polygon def plus haut
    dataset_MNT = dataset.clip(geometry)
    #ajout du mesh MNT sur le plot
    actor = pl.add_mesh(dataset_MNT,opacity=opa_topo,use_transparency=True,)
    


def create_mesh(solid):

    global pl, mesh_actor
    my_colormap = 'Spectral_r'
    size = 50
    Startpos = 5
    # my_colormap = 'cividis'
    if mesh_actor is not None:
        pl.remove_actor(mesh_actor)
    if solid == 'profile':  
        for i in range(len(colors)):
            actor = pl.add_mesh(mesh[i],scalars='Resistivity',cmap=my_colormap,
                    opacity=C[i],use_transparency=True,)
            # Make a separate callback for each widget
            callback = SetVisibilityCallback(actor)
            pl.add_checkbox_button_widget(
                callback,
                value=True,
                position=(5.0, Startpos),
                size=size,
                border_size=1,
                color_on=colors[i],
                color_off='grey',
                background_color='grey',
            )
            Startpos = Startpos + size + (size // 10)
        # for i, lst in enumerate(colors):
            # for j, color in enumerate(lst):
                # actor = pl.add_mesh(mesh1,scalars='Resistivity',cmap=my_colormap,
                # opacity=C1,use_transparency=True, color=color)
                # # Make a separate callback for each widget
                # callback = SetVisibilityCallback(actor)
                # pl.add_checkbox_button_widget(
                    # callback,
                    # value=True,
                    # position=(5.0, Startpos),
                    # size=size,
                    # border_size=1,
                    # color_on=color,
                    # color_off='grey',
                    # background_color='grey',
                # )
                # Startpos = Startpos + size + (size // 10)
        #img1=pl.add_mesh(mesh1,scalars='Resistivity',cmap=my_colormap,
            #opacity=C1,use_transparency=True,)
        
    
        #pl.add_mesh(mesh2,scalars='Resistivity',cmap=my_colormap,
            #opacity=C2,use_transparency=True,)
    pl.reset_camera()

# -----------------------------------------------------------------------------
# GUI
# -----------------------------------------------------------------------------




with SinglePageLayout(server) as layout:
    layout.title.set_text(state.trame__title)
    # create_mesh("profile")
    # print('create mesh effectue')
    # gps_coordonnee()
    # print('gps coor ok')
    # #polygon def for MNT
    # geometry = ee.Geometry.Polygon(
    # [[[gps_1],
      # [gps_2],
      # [gps_3],
      # [gps_4]]])
    # importation()
    # print('importation ok')
    
    with layout.toolbar:
        
        vuetify.VSpacer()
   #     vuetify.VBtn("Profile", click=lambda: create_mesh("profile"))

    with layout.content:
        with vuetify.VContainer(
            fluid=True,
            classes="pa-0 fill-height",
        ):
            # Use PyVista UI template for Plotters
            view = plotter_ui(pl)
            ctrl.view_update = view.update

server.start()
