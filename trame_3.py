import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QCheckBox


from trame.app import get_server
from trame.ui.vuetify import SinglePageLayout
from trame.widgets import vuetify
import numpy as np

import pyvista as pv
from pyvista.trame.ui import plotter_ui
from pv_utils import pvCoverageAlpha

# -----------------------------------------------------------------------------
# Trame initialization
# -----------------------------------------------------------------------------

pv.OFF_SCREEN = True

server = get_server()
state, ctrl = server.state, server.controller

state.trame__title = "Hplus fonctionnaire"
ctrl.on_server_ready.add(ctrl.view_update)

# -----------------------------------------------------------------------------
# Create object for the entire code
# -----------------------------------------------------------------------------

#obj tab pour stocker les noms de fichiers 
file_tot = ["vtk_files/ert1_DefaultInversion_shift.vtk","vtk_files/ert2_DefaultInversion_shift.vtk","vtk_files/ert3_DefaultInversion_shift.vtk","vtk_files/ert4_DefaultInversion_shift.vtk"]

#crea des mesh par application fonction sur le tab des noms de fichiers
mesh = pv.read(file_tot)

#color
my_colormap = 'Spectral_r'

#empty list for the application of coverafeAlpha
C = ['0','0','0','0']

#empty list for stocking pl.add_mesh and checkboxs
actor =[]
checkboxes = []


for j in range(len(mesh)):
    C[j]= pvCoverageAlpha(mesh[j])

### PLot with log colorscale

mesh_actor = None
solid = 'profile'
# -----------------------------------------------------------------------------
# Plotting
# -----------------------------------------------------------------------------
pl = pv.Plotter(window_size=(800, 600))
pl.set_background('lightgrey')
pl.show_bounds(
    grid='front',
    location='outer',
    all_edges=True,
    xlabel="Easting UTM 31N (m)",
    ylabel="Northing UTM 31N (m)",
    zlabel="Elevation (m)",
    color='black',
    fmt='%3i'
)
class Fenetre(QWidget):
    def create_mesh():

        global pl, mesh_actor, solid
        my_colormap = 'Spectral_r'
       # my_colormap = 'cividis'
        if mesh_actor is not None:
            pl.remove_actor(mesh_actor)
        if solid == 'profile':  
            #ajout des maillages a la fenetre
            print('ajout des maillages a la fenetre')
            for i in range(len(mesh)):
               actor.append(pl.add_mesh(mesh[i],scalars='Resistivity',cmap=my_colormap,opacity=C[i],use_transparency=True,))
               
        pl.reset_camera()     

    def __init__(self):
        QWidget.__init__(self)

        # création de la case à cocher
        self.case = QCheckBox(file_tot[0])

        # on connecte le signal "stateChanged" à la méthode "etat_change"
        self.case.stateChanged.connect(self.etat_change)
        
        # création du gestionnaire de mise en forme
        layout = QVBoxLayout()
        # ajout de la case à cocher au gestionnaire de mise en forme
        layout.addWidget(self.case)
        # on fixe le gestionnaire de mise en forme de la fenêtre
        self.setLayout(layout)

        self.setWindowTitle("Ma fenetre")

    # on définit une méthode à connecter au signal envoyé
    def etat_change(self):
        print("action sur la case")
        if self.case.checkState() == Qt.Checked:
            # pl.add_mesh(mesh[0],scalars='Resistivity',cmap=my_colormap,
                    # opacity=C[0],use_transparency=True,)
            print("coche")
        else:
            # pl.remove_actor(mesh[0])
            print("decoche") 

app = QApplication.instance() 
if not app:
    app = QApplication(sys.argv)
    
with SinglePageLayout(server) as layout:
    layout.title.set_text(state.trame__title)

    
    with layout.toolbar:
        
        vuetify.VSpacer()
        
    #     vuetify.VBtn("Profile", click=lambda: create_mesh("profile"))

    with layout.content:
        with vuetify.VContainer(
            fluid=True,
            classes="pa-0 fill-height",
        ):
            # Use PyVista UI template for Plotters
            view = plotter_ui(pl)
            ctrl.view_update = view.update
fen = Fenetre()
fen.show()
app.exec_()
server.start()
