#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 4 16:26:16 2023

@author: spasquet
"""

from trame.app import get_server
from trame.ui.vuetify import SinglePageLayout
from trame.ui.vuetify import SinglePageWithDrawerLayout
from trame.widgets import vuetify
import numpy as np

import pyvista as pv
from pyvista.trame.ui import plotter_ui
from pv_utils import pvCoverageAlpha

import pyproj 
import elevation

# -----------------------------------------------------------------------------
# Trame initialization
# -----------------------------------------------------------------------------

pv.OFF_SCREEN = True

server = get_server()
state, ctrl = server.state, server.controller

state.trame__title = "Hplus fonctionnaire"
ctrl.on_server_ready.add(ctrl.view_update)
# -----------------------------------------------------------------------------
# Init datafiles and topo files + mesh creation
# -----------------------------------------------------------------------------


#file1 = "vtk_files/WS_DD_profil1_Fonctionnaire_DefaultInversion_shift.vtk"
#file2 = "vtk_files/WS_DD_profil2_Fonctionnaire_DefaultInversion_shift.vtk"

#obj tab pour stocker les noms de fichiers + noms des mesh
file_tot = ['vtk_files/ert1_DefaultInversion_shift.vtk','vtk_files/ert2_DefaultInversion_shift.vtk'] #,'vtk_files/ert3_DefaultInversion_shift.vtk','vtk_files/ert4_DefaultInversion_shift.vtk']

topofile = ['topo/LarzacJasseERT_topo.txt']

name_mesh = []

num_btn = []

for k in range(len(file_tot)):
    temp = file_tot[k]
    # print(temp)
    temp = temp[10:]
    # print(temp)
    name_mesh.append(temp)
    

#crea des mesh par application fonction sur le tab des noms de fichiers
mesh = pv.read(file_tot)
taille =len(mesh)
print(taille)

#init opacity topo
opa_topo = 0.5
size = 50
Startpos = 5


C = ['0','0','0','0']

for j in range(len(mesh)):
    C[j]= pvCoverageAlpha(mesh[j])

actor = []

# -----------------------------------------------------------------------------
# Plotting
# -----------------------------------------------------------------------------

pl = pv.Plotter(window_size=(800, 600))
pl.set_background('lightgrey')
grid = pl.show_bounds(
    grid='front',
    location='outer',
    all_edges=True,
    xlabel="Easting UTM 31N (m)",
    ylabel="Northing UTM 31N (m)",
    zlabel="Elevation (m)",
    color='black',
    fmt='%3i'
)

# pl.add_ruler(label_color='black',tick_color='black')

### PLot with log colorscale

mesh_actor = None

#------------ fonction show hide----------------

class SetVisibilityCallback:
    """Helper callback to keep a reference to the actor being modified."""

    def __init__(self, actor):
        self.actor = actor

    def __call__(self, state):
        self.actor.SetVisibility(state)
        


        
#-----------------Coordonnees GPS 
utm_east = []
utm_north = []
gps_1 = []
gps_2 = []
gps_3 = []
gps_4 = []         

#-------------recuperation des coordo gps dans le fichier topo-------

def gps_coordonnee():
            
    for j in range(len(topofile)):
        # Ouvrir le fichier en lecture seule
        file = open(topofile[j], "r")
        # utiliser readlines pour lire toutes les lignes du fichier
        # La variable "lignes" est une liste contenant toutes les lignes du fichier
        lines = file.readlines()
        # fermez le fichier après avoir lu les lignes
        file.close()

        #decoupe la premiere ligne
        first = lines[0].split()

        #decoupe de la derniere ligne
        num_end = len(lines)
        #print(num_end)
        end  = lines[num_end-1].split()
        #recuperation des coordonnees east et north
        # print(first[1])
        utm_east.insert(j,float(first[1]))
        utm_north.insert(j,float(first[2]))

        utm_east.insert(j,float(end[1]))
        utm_north.insert(j,float(end[2]))
    # print(utm_east)
    # print(utm_north)
    gps_1.insert(1,float(min(utm_east)))
    gps_1.insert(2,float(max(utm_north)))
    gps_2.insert(1,float(max(utm_east)))
    gps_2.insert(2,float(max(utm_north)))
    gps_3.insert(1,float(min(utm_east)))
    gps_3.insert(2,float(min(utm_north)))
    gps_4.insert(1,float(max(utm_east)))
    gps_4.insert(2,float(min(utm_north)))
    # print(gps_1)
    # print(gps_2)
    # print(gps_3)
    # print(gps_4)
    
#-------- tableaux de couple de valeurs pour les coordo utm et log lat ------
coordo_utm = [ gps_1,
               gps_2,
               gps_3,
               gps_4]
               
coordo_lonlat = [ gps_1,
               gps_2,
               gps_3,
               gps_4]

P = pyproj.Proj(proj='utm', zone=31, ellps='WGS84', preserve_units=True)
#----------creation d'une fonction de transfert utm -> longlat ----------
def to_lat_long(coordo_utm, coordo_lonlat):
#creation d'un objet projet 
    
    print('utm')
    print(coordo_utm)
    for i in[0,1,2,3] : #boucle bornée car tjrs 4 coordonnees, 4 points 
        print(coordo_utm[i])
        (lon, lat )= P(coordo_utm[i][0], coordo_utm[i][1], inverse=True)
        coordo_lonlat[i][0]=lon
        coordo_lonlat[i][1]=lat
        print(coordo_lonlat[i][0], coordo_lonlat[i][1])
    

  
def importation(coordo_lonlat):  
    # clip the SRTM1 30m DEM of Rome and save it to Rome-DEM.tif
    # elevation.clip(bounds=(coordo_lonlat[0][0], coordo_lonlat[0][1], coordo_lonlat[1][0], coordo_lonlat[1][1]), output='Rome-DEM.tif')
    elevation.clip(bounds=(12.35, 41.8, 12.65, 42), output='Rome-DEM.tif')
    # clean up stale temporary files and fix the cache in the event of a server error
    elevation.clean()

def create_mesh(actor):

    global pl, mesh_actor
    my_colormap = 'Spectral_r'
    size = 50
    Startpos = 5
    # my_colormap = 'cividis'
    if mesh_actor is not None:
        pl.remove_actor(mesh_actor)
    else:
        for i in range(len(mesh)):
            mesh_add = pl.add_mesh(mesh[i],scalars='Resistivity',cmap=my_colormap,
                    opacity=C[i],use_transparency=True)
            actor.append(mesh_add)
            # print(actor[i])
        
    pl.reset_camera()
#-------------fonction setvisibility -------
#avec la valeur de flag la fonction affiche ou cache le mesh
def remove_profil(num,flag):
    # pl.remove_actor(actor[num])
    # pl.update()
    # pl.show()
    actor[num].SetVisibility(flag)
    # pl.update()
    # pl.show()
    
    
#---------- Github help ----
def update_profile(index, visible):
    state.profileVisibility[index] = visible
    state.dirty("profileVisibility")
    # do you show/hide
 
def control_grid(flag):
    if flag =='0':
        pl.show_bounds(
            grid='front',
            location='outer',
            all_edges=True,
            xlabel="Easting UTM 31N (m)",
            ylabel="Northing UTM 31N (m)",
            zlabel="Elevation (m)",
            color='lightgrey',
            fmt='%3i'
        )
    elif flag =='1':
        print('youplalalalal')
   
    pl.update()
    pl.show()
#---------------------------------
# GUI
# -----------------------------------------------------------------------------
@state.change("visible")
def on_visible_change(visible, **kwargs):
    if visible:
        remove_profil(0,True)
    else:
        remove_profil(0,False)


state.profileVisibility = [True, True, True, True, True]


with SinglePageWithDrawerLayout(server) as layout:
    
    layout.title.set_text(state.trame__title)
    create_mesh(actor)
    # print('lancement de gsp coordo')
    # gps_coordonnee()
    # print(coordo_utm)
    # print('lancement de utm -> long lat')
    # to_lat_long(coordo_utm, coordo_lonlat)
    # print(coordo_lonlat)
    # print('lancement de limportation')
    # importation(coordo_lonlat)
    # print('importation ok')
    # print(actor)
    # vuetify.VBtn("hide", click=lambda: print('youplala'), style="position: relative; top: 5; right:0 ; z-index: 20;")
    
    with layout.toolbar:
        vuetify.VSpacer()  
         
    with layout.drawer:
        vuetify.VSpacer()
        # vuetify.VCheckbox(label = 'essai', v_model=("visible", True))

        # And then

 
                
        # vuetify.VCheckbox(label = file_tot[0], true_value = remove_profil(0,1)), false_value = remove_profil(0,0))
        
        # vuetify.VBtn(name_mesh[0], click=lambda: remove_profil(0,1))#,style="position: absolute; top: 100; right: 50; z-index: 10;",)
        # vuetify.VBtn("hide", click=lambda: remove_profil(0,0))
        # vuetify.VBtn(name_mesh[1], click=lambda: remove_profil(1,1)) 
        # vuetify.VBtn("hide", click=lambda: remove_profil(1,0))
        
        vuetify.VCheckbox(
           v_for="v, i in profileVisibility",
           key="i",
           label=("Toggle visibility of {{ i }}",),
           value=("v",),
           change=(update_profile, "[i, $event]"),
        )
        
    with layout.content:
        with vuetify.VContainer(
        
            fluid=True,
            classes="pa-0 fill-height",
        ):
            
            # Use PyVista UI template for Plotters
            view = plotter_ui(pl)
            ctrl.view_update = view.update

server.start()
