# pyvista_trame 3D visualization

Simple 3D visualization of VTK geophysical profiles (ERT) using pyvista and trame

Follows examples found in:
- https://github.com/pyvista/pyvista/tree/main/examples_trame
- https://github.com/pyvista/pyvista/discussions/4297

Ideas originated after reading these articles and discussions : 
- https://discourse.vtk.org/t/pyvista-trame-jupyter-3d-visualization/10610
- https://towardsdatascience.com/3d-mesh-models-in-the-browser-using-python-dash-vtk-e15cbf36a132
- https://github.com/shkiefer/dash_vtk_unstructured/issues/1

## Installation 

See `environment.yml` for required packages.
Latest versions of [pyvista](https://docs.pyvista.org/) and [trame](https://kitware.github.io/trame/) are necessary.
`trame` is only available with `pip`. If working with anaconda, first install `pip` in your conda environment, then `pip install trame`.

## Examples

### vtk_viewer.py

Basic example to visualize vtk file (geophysical profiles in `/vtk_files`)

### mesh_viewer.py

More complex example to visualize a mesh from 3 txt files (in `/mesh_files`). Provides interesting examples of GUI options (data picker, threshold bar)

## Advanced use of trame

Trame tutorial can be found here : https://kitware.github.io/trame/docs/tutorial.html
