import sys
import pyvista as pv
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QCheckBox
from PyQt5.QtCore import QThread
from multiprocessing import Process, Queue

from trame.app import get_server
from trame.ui.vuetify import SinglePageLayout
from trame.widgets import vuetify
from pv_utils import pvCoverageAlpha
import numpy as np
# -----------------------------------------------------------------------------
# Trame initialization
# -----------------------------------------------------------------------------

pv.OFF_SCREEN = True

server = get_server()
state, ctrl = server.state, server.controller

state.trame__title = "Hplus fonctionnaire"
ctrl.on_server_ready.add(ctrl.view_update)

# -----------------------------------------------------------------------------
# Create object for the entire code
# -----------------------------------------------------------------------------

#obj tab pour stocker les noms de fichiers 
file_tot = ["vtk_files/ert1_DefaultInversion_shift.vtk","vtk_files/ert2_DefaultInversion_shift.vtk","vtk_files/ert3_DefaultInversion_shift.vtk","vtk_files/ert4_DefaultInversion_shift.vtk"]

#crea des mesh par application fonction sur le tab des noms de fichiers
mesh = pv.read(file_tot)

#color
my_colormap = 'Spectral_r'

#empty list for the application of coverafeAlpha
C = ['0','0','0','0']

#empty list for stocking pl.add_mesh and checkboxs
actor =[]
checkboxes = []


for j in range(len(mesh)):
    C[j]= pvCoverageAlpha(mesh[j])

### PLot with log colorscale

mesh_actor = None
# -----------------------------------------------------------------------------
# Plotting
# -----------------------------------------------------------------------------
pl = pv.Plotter(window_size=(800, 600))
pl.set_background('lightgrey')
pl.show_bounds(
    grid='front',
    location='outer',
    all_edges=True,
    xlabel="Easting UTM 31N (m)",
    ylabel="Northing UTM 31N (m)",
    zlabel="Elevation (m)",
    color='black',
    fmt='%3i'
)

# création du gestionnaire de mise en forme
layout_ = QVBoxLayout()

def create_mesh(solid):

    global pl, mesh_actor
    my_colormap = 'Spectral_r'
   # my_colormap = 'cividis'
    if mesh_actor is not None:
        pl.remove_actor(mesh_actor)
    if solid == 'profile':  
        #ajout des maillages a la fenetre
        print('ajout des maillages a la fenetre')
        for i in range(len(mesh)):
           actor.append(pl.add_mesh(mesh[i],scalars='Resistivity',cmap=my_colormap,opacity=C[i],use_transparency=True,))
           
    pl.reset_camera()       

# Fonction de slot pour contrôler l'affichage des maillages
def checkbox_state_changed():
    for j in range(len(mesh)):
        print(j)
        actor[j].SetVisibility(checkboxes[j].isChecked())
        print('gérer le chgmt etat')
    pl.render()

def create_checkbox():
    #creation de toutes les checkbox
    print('creation de toutes les checkbox')

    for i in range(len(mesh)):
        print('boucle des cases')
        checkbox = QCheckBox(file_tot[i])
        checkboxes.append(checkbox)
        
        

def connect_boxes():
    for checkbox in checkboxes:
        checkbox.stateChanged.connect(checkbox_state_changed)
        layout_.addWidget(checkbox)
        


# Créer la classe pour la fenêtre principale de PyQt5
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Interface et Affichage 3D")
        self.setGeometry(100, 100, 800, 600)
        # Initialiser le widget PyVista pour l'affichage 3D
        self.plotter = pv.Plotter(self)
        self.setCentralWidget(self.plotter.ren_win)

# Fonction pour exécuter l'interface PyQt5
def run_pyqt():

    app = QApplication(sys.argv)
    create_checkbox()
    connect_boxes()
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())

# Fonction pour exécuter l'affichage PyVista
def run_pyvista(queue):
    
    create_mesh("profile")
    # Ajouter du code supplémentaire pour manipuler l'affichage
    queue.put(pl.ren_win)
    pl.show()

if __name__ == '__main__':
    # Créer une file d'attente pour communiquer entre les processus
    queue = Queue()

    # Créer un processus pour exécuter l'interface PyQt5
    pyqt_process = Process(target=run_pyqt)

    # Créer un processus pour exécuter l'affichage PyVista
    pyvista_process = Process(target=run_pyvista, args=(queue,))

    # Démarrer les processus
    pyqt_process.start()
    pyvista_process.start()

    # Récupérer la fenêtre PyVista depuis la file d'attente
    ren_win = queue.get()

    # Configurer le widget PyVista dans l'interface PyQt5
    app = QApplication(sys.argv)
    window = MainWindow()
    window.plotter.ren_win = ren_win
    window.show()

    # Attendre que les processus se terminent
    pyqt_process.join()
    pyvista_process.join()

    sys.exit(app.exec_())
