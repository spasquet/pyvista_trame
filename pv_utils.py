#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 16:26:16 2023

@author: spasquet
"""
import numpy as np

def pvCoverageAlpha(mesh):
    dropThreshold=0.1
    C = np.asarray(mesh["Coverage"])
    if (np.min(C) < 0.) | (np.max(C) > 1.) | (np.max(C) < 0.5):

        nn, hh = np.histogram(C, 50)
        nnn = nn.cumsum(axis=0) / float(len(C))

        #        print("min-max nnn ", min(nnn), max(nnn))
        mi = hh[np.min(np.where(nnn > 0.02)[0])]

        if np.min(nnn) > dropThreshold:
            ma = np.max(C)
        else:
            ma = hh[np.max(np.where(nnn < dropThreshold)[0])]

        C = (C - mi) / (ma - mi)
        C[np.where(C < 0.)] = 0.0
        C[np.where(C > 0.95)] = 1.0
    C = 1-C
    return C
