#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 09:06:17 2023

@author: agauvain
"""
import whitebox
wbt = whitebox.WhiteboxTools()
wbt.set_compress_rasters(True)
wbt.verbose = False
import rasterio
import geopandas as gpd
from shapely.geometry import Polygon


topofile = ['topo/LarzacJasseERT_topo.txt']
taille = len(topofile)

######Coordonnees GPS 
# List des coordonnées de tes données geophy
x = []
y = []
     

        
for j in range(taille):
    # Ouvrir le fichier en lecture seule
    file = open(topofile[j], "r")
    # utiliser readlines pour lire toutes les lignes du fichier
    # La variable "lignes" est une liste contenant toutes les lignes du fichier
    lines = file.readlines()
    # fermez le fichier après avoir lu les lignes
    file.close()

    #decoupe la premiere ligne
    first = lines[0].split()

    #decoupe de la derniere ligne
    num_end = len(lines)
    print(num_end)
    end  = lines[num_end-1].split()
    #recuperation des coordonnees east et north
    print(first[1])
    x.insert(j,float(first[1]))
    y.insert(j,float(first[2]))

    x.insert(j,float(end[1]))
    y.insert(j,float(end[2]))
   
   
print(x)
print(y)


# Création de l'emprise de tes données géophy
points_x = [min(x), max(x), max(x), min(x), min(x)]
points_y = [min(y), min(y), max(y), max(y), min(y)]

print(points_x)
print(points_y)
# Définition du système de projection. Tes données et le MNT doivent être dans le même système
epsg = 'epsg:900913'
#https://learnosm.org/fr/advanced/projections-and-files-format/

# Création d'un shapefile polygon comme masque pour extraire la partie du MNT que tu souhaites
polygon_geom = Polygon(zip(points_x, points_y))
polygon = gpd.GeoDataFrame(index=[0], crs=epsg, geometry=[polygon_geom])       

# Sauvegarde du shapefile polygon.shp
shp_path = 'd:/Documents/Etudes/Cours/Master/Cours/Stage M1/Stage/codes_pybert_2/pyvista_trame-main/pyvista_trame-main/fichiers_MNT/polygon.shp'
polygon.to_file(filename='polygon.shp', driver="ESRI Shapefile")

# Clip du MNT par rapport au shapefile
dem_path = 'd:/Documents/Etudes/Cours/Master/Cours/Stage M1/Stage/codes_pybert_2/pyvista_trame-main/pyvista_trame-main/fichiers_MNT/raster_1.tif'
clip_dem_path = 'd:/Documents/Etudes/Cours/Master/Cours/Stage M1/Stage/codes_pybert_2/pyvista_trame-main/pyvista_trame-main/fichiers_MNT/raster_clip.tif'
wbt.clip_raster_to_polygon(dem_path, shp_path, clip_dem_path)

# Load du MNT
src = rasterio.open(clip_dem_path)
dem_data_clip = src.read(1)